<?php

namespace BcWbJmGsbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignefraisforfait
 *
 * @ORM\Table(name="lignefraisforfait", uniqueConstraints={@ORM\UniqueConstraint(name="idVisiteur", columns={"idVisiteur", "mois", "idFraisForfait"})}, indexes={@ORM\Index(name="idFraisForfait", columns={"idFraisForfait"}), @ORM\Index(name="lignefraisforfait_ibfk_1", columns={"idVisiteur"})})
 * @ORM\Entity(repositoryClass="BcWbJmGsbBundle\Entity\LignefraisforfaitRepository")

 */
class Lignefraisforfait
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=6, nullable=false)
     */
    private $mois;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var \Fichefrais
     *
     * @ORM\ManyToOne(targetEntity="Fichefrais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idVisiteur", referencedColumnName="idVisiteur")
     * })
     */
    private $idvisiteur;

    /**
     * @var \Fraisforfait
     *
     * @ORM\ManyToOne(targetEntity="Fraisforfait")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFraisForfait", referencedColumnName="id")
     * })
     */
    private $idfraisforfait;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return Lignefraisforfait
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Lignefraisforfait
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set idvisiteur
     *
     * @param \BcWbJmGsbBundle\Entity\Fichefrais $idvisiteur
     *
     * @return Lignefraisforfait
     */
    public function setIdvisiteur(\BcWbJmGsbBundle\Entity\Fichefrais $idvisiteur = null)
    {
        $this->idvisiteur = $idvisiteur;

        return $this;
    }

    /**
     * Get idvisiteur
     *
     * @return \BcWbJmGsbBundle\Entity\Fichefrais
     */
    public function getIdvisiteur()
    {
        return $this->idvisiteur;
    }

    /**
     * Set idfraisforfait
     *
     * @param \BcWbJmGsbBundle\Entity\Fraisforfait $idfraisforfait
     *
     * @return Lignefraisforfait
     */
    public function setIdfraisforfait(\BcWbJmGsbBundle\Entity\Fraisforfait $idfraisforfait = null)
    {
        $this->idfraisforfait = $idfraisforfait;

        return $this;
    }

    /**
     * Get idfraisforfait
     *
     * @return \BcWbJmGsbBundle\Entity\Fraisforfait
     */
    public function getIdfraisforfait()
    {
        return $this->idfraisforfait;
    }
}
