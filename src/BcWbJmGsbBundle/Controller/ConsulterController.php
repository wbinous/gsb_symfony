<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BcWbJmGsbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
/**
 * Description of ConsulterController
 *
 * @author developpeur
 */
class ConsulterController extends Controller{

    //put your code here
    

    public function consulterMoisAction(SessionInterface $session){
        $idVisiteur = $session->get('idVisiteur');
        $fichefrais_repository= $this->getDoctrine()->getRepository('BcWbJmGsbBundle:Fichefrais');
        $liste_fichefrais = $fichefrais_repository->findBy(array('idvisiteur'=>$idVisiteur));
        return $this->render('@BcWbJmGsb/Consultation/consulterMois.html.twig',
                array('listefichefrais'=>$liste_fichefrais));
    }

}
