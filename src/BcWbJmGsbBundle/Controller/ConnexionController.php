<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BcWbJmGsbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BcWbJmGsbBundle\Entity\Visiteur;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
/**
 * Description of connexionController
 *
 * @author developpeur
 */
class ConnexionController extends Controller{
    
    //put your code here
    public function connecterAction(Request $request,SessionInterface $session)
    {
        $visiteur = new Visiteur();
       /* $form = $this->createForm(new VisiteurType(),$connexion);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
          return $this->render('@BcWbJmGsb/Connexion/connecter.html.twig');
        */
        $form = $this->createFormBuilder($visiteur)
        ->add('login', TextType::class)
        ->add('mdp', PasswordType::class)
        ->add('valider', SubmitType::class)
        ->add('annuler', ResetType::class)
        ->getForm();
        $form->handleRequest($request) ;
            
        if ($form->isSubmitted()) {
            $login = $form->get('login')->getData();
            $mdp = md5($form->get('mdp')->getData());        
        $repository_visiteur = $this->getDoctrine()->getManager()->getRepository('BcWbJmGsbBundle:Visiteur');
        $visiteur = $repository_visiteur->findOneBy(array('login'=>$login,'mdp'=>$mdp,'comptable'=>0));
        
        $session->set('idVisiteur',$visiteur->getIdvisiteur()) ;
        return $this->render('@BcWbJmGsb/Connexion/validConnexion.html.twig',
                array('visiteur'=>$visiteur));
         }
        return $this->render('@BcWbJmGsb/Connexion/connecter.html.twig',array('form'=>$form->createView()));
        
        }
        
    
    public function validerConnexionAction(){
        $login = $_REQUEST['login'];
        $mdp = $_REQUEST['mdp'];
        
       $repository_visiteur = $this->getDoctrine()->getManager()->getRepository('BcWbJmGsbBundle:Visiteur');
       $visiteur = $repository_visiteur->findByLoginAndMdp($login,$mdp);
       
       return $visiteur->render('@BcWbJmGsb\Connexion\index.html.twig');
                       
  
    }

    public function deconnecterAction(){
        /*
         * mettre ici le code pour deconnecter l'utilisateur
         */
        return $this->render('@BcWbJmGsb\Connexion\connecter.html.twig');
    }
        
}
